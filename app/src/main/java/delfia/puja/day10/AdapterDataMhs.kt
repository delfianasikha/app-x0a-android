package delfia.puja.day10

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso


class AdapterDataMhs(val dataMhs: List<HashMap<String,String>>) : RecyclerView.Adapter<AdapterDataMhs.HolderDataMhs> (){
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterDataMhs.HolderDataMhs {
        val v  =LayoutInflater.from(parent.context).inflate(R.layout.row_mhs,parent,false)
        return HolderDataMhs(v)
    }

    override fun getItemCount(): Int {
        return dataMhs.size
    }

    override fun onBindViewHolder(holder: AdapterDataMhs.HolderDataMhs, position: Int) {
        val data =dataMhs.get(position)
        holder.txnim.setText(data.get("nim"))
        holder.txnama.setText(data.get("nama"))
        holder.txProd.setText(data.get("nama_prodi"))
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.photo)
        holder.tgl.setText(data.get("tanggal_lahir"))
        holder.jenis_kel.setText(data.get("jenis_kelamin"))
        holder.alamat.setText(data.get("alamat"))
    }

    class HolderDataMhs(v : View) :  RecyclerView.ViewHolder(v){
        val txnim = v.findViewById<TextView>(R.id.nim)
        val txnama = v.findViewById<TextView>(R.id.namaMhs)
        val txProd = v.findViewById<TextView>(R.id.Prodi)
        val photo = v.findViewById<ImageView>(R.id.ImgUpload)
        val tgl =v.findViewById<TextView>(R.id.txTgl)
        val jenis_kel  = v.findViewById<TextView>(R.id.txJenis_kel)
        val alamat = v.findViewById<TextView>(R.id.txAlamat)
    }

}